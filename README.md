# Bilans du Crans

Ce dépôt est dédié à l'hébergement des différents bilans moraux, financiers et
techniques des différentes années. Les sources sont privilégiées à l'avenir,
mais les PDF peuvent être exceptionnellement ajoutés au dépôt si les sources
sont inaccessibles.

Les compte-rendus des différentes assemblées générales peuvent être trouvés
sur le wiki : https://wiki.crans.org/ComptesRendusCrans.
